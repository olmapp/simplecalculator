import React from "react";
import { getLocales } from "react-native-localize";
import i18n from "i18n-js";
// import memoize from "lodash.memoize";

import { I18nManager } from "react-native";


const translationGetters = {
  // lazy requires (metro bundler does not support symlinks)
  "en-US": () => require("../translations/en-US.json"),
  "pl-PL": () => require("../translations/pl-PL.json"),
};

const availableTranslations = Object.keys(translationGetters)

export const translate = (stringId) => {
  return i18n.t(stringId)
}

export const initializeTranslations = () => {
  setI18nConfig();
}

const setI18nConfig = () => {

  const defaultLang = { languageTag: "en-US", isRTL: false }

  let prefferredLangs = getLocales().filter(langSpec => availableTranslations.includes(langSpec.languageTag))
  if (prefferredLangs.length === 0) prefferredLangs.push(defaultLang)

  console.log("Preferred langs", prefferredLangs)
  const bestLang = prefferredLangs[0]

  console.log("translator init, best lang is", bestLang);

  // clear translation cache
  // translate.cache.clear();
  // update layout direction
  I18nManager.forceRTL(bestLang.isRTL);

  // set i18n-js config
  i18n.translations = { [bestLang.languageTag]: translationGetters[bestLang.languageTag]() };
  i18n.locale = bestLang.languageTag;
};
