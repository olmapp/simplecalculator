import Calculator from "../Calculator";

it('worksWithAddition', () => {
  const calculator = new Calculator()

  calculator.modOperand(true, 2)

  expect(calculator.res).toBe(2)

  calculator.modOperand(false, 2)

  expect(calculator.res).toBe(4)

  calculator.modResult(20)

  expect(calculator.a).toBe(18)
  expect(calculator.b).toBe(2)
})

it('worksWithSubsctraction', () => {
  const calculator = new Calculator()
  calculator.modOperator("-");

  calculator.modOperand(true, 2)

  expect(calculator.res).toBe(2)

  calculator.modOperand(false, 2)

  expect(calculator.res).toBe(0)

  calculator.modResult(20)

  expect(calculator.a).toBe(22)
  expect(calculator.b).toBe(2)
})

it('worksWithMultiplication', () => {
  const calculator = new Calculator()
  calculator.modOperator("*");

  calculator.modOperand(true, 2)

  expect(calculator.res).toBe(0)

  calculator.modOperand(false, 2)

  expect(calculator.res).toBe(4)

  calculator.modResult(20)

  expect(calculator.a).toBe(1)
  expect(calculator.b).toBe(20)
})

it('worksWithDivision', () => {
  const calculator = new Calculator()
  calculator.modOperator("/");

  calculator.modOperand(true, 2)

  expect(calculator.res).toBe(0)

  calculator.modOperand(false, 2)

  expect(calculator.res).toBe(1)

  calculator.modResult(20)

  expect(calculator.a).toBe(20)
  expect(calculator.b).toBe(1)
})

it('worksWhenSwitchingOperator', () => {
  const calculator = new Calculator()

  calculator.modOperand(true, 2)

  expect(calculator.res).toBe(2)

  calculator.modOperator("*")

  expect(calculator.res).toBe(0)
  calculator.modOperand(false, 5)

  calculator.modOperator("/")
  expect(calculator.res).toBe(0)

  calculator.modOperator("-")
  expect(calculator.res).toBe(-3)
})

it('canEnsureProperState', () => {
  const calculator = new Calculator()

  calculator.modOperand(true, 2)
  expect(calculator.res).toBe(2)

  calculator.modOperand(true, undefined)
  expect(calculator.res).toBe(2)

  expect(calculator.ensureState()).toBe(true)
  expect(calculator.res).toBe(0)
  expect(calculator.a).toBe(0)


})