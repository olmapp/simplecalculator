
const allowedOperators = ["+", "-", "*", "/"]

export default class Calculator {

  constructor() {
    this.a = 0
    this.b = 0
    this.res = 0
    this.operator = "+"
  }

  modResult = (newResult) => {
    this.res = newResult

    if (newResult === undefined) return;

    if (this.operator === "+") {
      this.a = newResult - this.b
    } else if (this.operator === "-") {
      this.a = newResult + this.b
    } else if (this.operator === "*") {
      this.a = 1
      this.b = newResult
    } else if (this.operator === "/") {
      this.a = newResult
      this.b = 1
    }
  }

  modOperand = (isLeft, newVal) => {
    if (isLeft) this.a = newVal
    else this.b = newVal

    if (newVal === undefined) return

    this.computeResult()
  }

  modOperator = (newOperator) => {
    if (allowedOperators.includes(newOperator)) {
      this.operator = newOperator
    } else {
      this.operator = "+"
    }

    this.computeResult()
  }

  ensureState = () => {

    let hasChanged = false;

    if (this.a === undefined) {
      this.a = 0
      hasChanged = true
    }
    if (this.b === undefined) {
      this.b = 0
      hasChanged = true
    }
    if (this.res === undefined) {
      this.res = 0
      hasChanged = true
    }

    this.computeResult()

    return hasChanged
  }

  computeResult = () => {
    if (this.operator === "+") {
      this.res = this.a + this.b
    } else if (this.operator === "-") {
      this.res = this.a - this.b
    } else if (this.operator === "*") {
      this.res = this.a * this.b
    } else if (this.operator === "/") {
      if (this.b !== 0)
        this.res = Math.round(this.a / this.b)
      else
        this.res = 0
    }
  }

  getRandom = (upTo) => {
    return Math.floor(Math.random() * upTo + 1);
  }

  randomize = () => {
    this.a = this.getRandom(200)
    this.b = this.getRandom(200)
    this.operator = allowedOperators[this.getRandom(allowedOperators.length) - 1]

    this.computeResult()
  }
}