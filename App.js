/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, SafeAreaView } from 'react-native';
import CalculatorComponent from './Components/CalculatorComponent';
import PastComputationsComponent from './Components/PastComputationsComponent';
import { initializeTranslations } from './Translator/Translator';

initializeTranslations();

export default class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      pastComputations: []
    }
  }

  onSaveComputation = (pastComputation) => {
    let pastComputations = [{ computation: pastComputation }, ...this.state.pastComputations]
    this.setState({ pastComputations })
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <CalculatorComponent style={styles.halfScreen} onSaveComputation={this.onSaveComputation} />
        <PastComputationsComponent style={styles.halfScreen} pastComputations={this.state.pastComputations} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fafafa',
    flexDirection: 'column'
  },
  halfScreen: {
    flex: 1
  },
});
