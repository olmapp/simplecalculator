/**
 * @format
 */
import { NativeModules, NativeEventEmitter } from 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('NativeEventEmitter');
jest.mock('react-native-localize');

NativeModules.RNLocalize = {
  initialConstants: jest.fn()
}
