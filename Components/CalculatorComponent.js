import React, { Component } from 'react';
// import { styles, colors } from './Styles';
import { TextInput, StyleSheet, View, Text, Picker, TouchableOpacity } from 'react-native';
import Calculator from '../Calculator/Calculator';
import { translate } from '../Translator/Translator';



const styles = StyleSheet.create({

  horizontalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  calculatorInput: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    backgroundColor: "#fafafa",
    flex: 0.3
  },
  operatorPicker: {
    width: 85,
    height: 70,
    // margin: 10,
    // flex: 0.3,
    justifyContent: 'center',
    textAlign: 'center'
  },
  borderedButton: {
    borderRadius: 6,
    borderColor: 'green',
    borderWidth: 2,
    padding: 8,
    marginBottom: 4,
  }
});


export default class CalculatorComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      calculator: new Calculator()
    }

  }

  onValueChanged = (position, newValue) => {
    console.log("on value changed pos", position, "new val", newValue);

    if (isNaN(newValue)) {
      console.log("on value changed - is nan");

      return
    }

    let newValueAsNumber = undefined;
    if (!newValue || newValue === "") {
      console.log("on value changed - empty value")
    } else {
      newValueAsNumber = Number(newValue)
    }
    const calculator = this.state.calculator

    if (position == 0) {
      calculator.modOperand(true, newValueAsNumber)
    } else if (position == 1) {
      calculator.modOperand(false, newValueAsNumber)
    } else {
      calculator.modResult(newValueAsNumber)
    }

    this.setState({
      calculator
    });
  }

  onOperatorChanged = (newOperator) => {
    const calculator = this.state.calculator

    calculator.ensureState()

    calculator.modOperator(newOperator)
    this.setState({ calculator })
  }

  ensureCalculatorState = () => {
    const calculator = this.state.calculator
    const hasChanged = calculator.ensureState()

    if (hasChanged) this.setState({ calculator })
  }

  getValueRepresentation = (value) => {
    if (value === undefined) return ""

    return "" + value
  }

  resetState = () => {
    const calculator = this.state.calculator

    calculator.a = 0
    calculator.b = 0
    calculator.res = 0
    calculator.operator = "+"

    this.setState({ calculator })
  }

  onSaveComputation = () => {
    this.ensureCalculatorState()
    const calculator = this.state.calculator

    let computation = {
      a: calculator.a,
      b: calculator.b,
      res: calculator.res,
      operator: calculator.operator
    }

    this.props.onSaveComputation(computation)
    this.resetState()
  }

  onRandomize = () => {
    const calculator = this.state.calculator
    calculator.randomize()

    this.setState({ calculator })
  }

  render() {
    console.log("render calculator component");

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={styles.horizontalContainer}>
          <TextInput maxLength={4} keyboardType='numeric' style={styles.calculatorInput} value={this.getValueRepresentation(this.state.calculator.a)} onChangeText={this.onValueChanged.bind(this, 0)} onBlur={this.ensureCalculatorState} />
          <Picker
            selectedValue={this.state.calculator.operator}
            style={styles.operatorPicker}
            mode="dropdown"
            onValueChange={this.onOperatorChanged}>
            <Picker.Item label="+" value="+" />
            <Picker.Item label="-" value="-" />
            <Picker.Item label="*" value="*" />
            <Picker.Item label="/" value="/" />
          </Picker>
          <TextInput maxLength={4} keyboardType='numeric' style={styles.calculatorInput} value={this.getValueRepresentation(this.state.calculator.b)} onChangeText={this.onValueChanged.bind(this, 1)} onBlur={this.ensureCalculatorState} />
          <Text>=</Text>
          <TextInput maxLength={8} keyboardType='numeric' style={{ ...styles.calculatorInput, flex: 0.5 }} value={this.getValueRepresentation(this.state.calculator.res)} onChangeText={this.onValueChanged.bind(this, 2)} onBlur={this.ensureCalculatorState} />
        </View>
        <View style={styles.horizontalContainer}>

          <TouchableOpacity onPress={this.onRandomize} style={ {...styles.borderedButton, marginRight: 8 }}>
            <Text>{translate("randomize")}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onSaveComputation} style={styles.borderedButton}>
            <Text>{translate("save_computation")}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

}
