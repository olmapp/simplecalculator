import React, { Component } from 'react';
// import { styles, colors } from './Styles';
import { TextInput, StyleSheet, View, Text, Picker, FlatList } from 'react-native';
import Calculator from '../Calculator/Calculator';



const styles = StyleSheet.create({

  horizontalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  calculatorInput: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    backgroundColor: "#fafafa",
    flex: 0.3
  },
  operatorPicker: {
    width: 30,
    height: 30,
    // margin: 10,
    flex: 0.3,
    justifyContent: 'center'
  },
  centeredText: {
    textAlign: 'center',
    color: '#333333',
  },
});


export default class PastComputationsComponent extends Component {

  constructor(props) {
    super(props);
  }


  renderItem = ({ item }) => {
    const computation = item.computation;

    return (
      <Text style={styles.centeredText}>
        {item.computation.a} {item.computation.operator} {item.computation.b} = {item.computation.res}
      </Text>
    )
  }

  keyExtractor = (item, index) => ""+index

  render() {
    console.log("render computations component");

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

        <FlatList
          data={this.props.pastComputations}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}

        />
      </View>

    );
  }

}
