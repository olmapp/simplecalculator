/**
 * @format
 */

import 'react-native';
import { NativeModules } from 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import CalculatorComponent from '../CalculatorComponent';


it('renders correctly', () => {
  const app = renderer.create(<CalculatorComponent />);  

  expect(app.toJSON()).toMatchSnapshot();
});
