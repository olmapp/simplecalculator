/**
 * @format
 */

import 'react-native';
import { NativeModules } from 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import PastComputationsComponent from '../PastComputationsComponent';



it('renders correctly without past computations', () => {
  const app = renderer.create(<PastComputationsComponent pastComputations={[]} />);

  expect(app.toJSON()).toMatchSnapshot();
});

it('renders correctly with past computations', () => {
  const app = renderer.create(<PastComputationsComponent pastComputations={[{ computation: { a: 1, b: 1, operator: "+", res: 2 } }]} />);

  expect(app.toJSON()).toMatchSnapshot();
});